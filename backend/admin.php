<?php

function wpdocs_register_my_custom_menu_page() {
    add_menu_page (
        __( 'Display Music Albums', 'display-music-albums' ),
        'Albums',
        'manage_options',
        'albums-options-page',
        'albums_page_contents',
        'dashicons-album',
        6
    );
}
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );

function albums_page_contents() { ?>
    <h1><?php echo __("Display Music Albums","display-music-albums"); ?></h1>
    <form method="POST" action="options.php">
        <?php
            settings_fields( 'albums-options-page' );
            do_settings_sections( 'albums-options-page' );
            submit_button();
        ?>
    </form>
<?php }

function album_page_settings() {

    add_settings_section(
        'albums_options_settings_section',
        __( 'Albums options', 'my-textdomain' ),
        '',
        'albums-options-page'
    );

    add_settings_field(
        "artist_id",
        __( 'Artist Spotify ID', 'display-music-albums' ),
        'artist_id_markup',
        'albums-options-page',
        'albums_options_settings_section'
    );
    register_setting( 'albums-options-page', 'artist_id' );

    add_settings_field(
        "spotify_token",
        __( 'Spotify API Token', 'display-music-albums' ),
        'spotify_token_markup',
        'albums-options-page',
        'albums_options_settings_section'
    );
    register_setting( 'albums-options-page', 'spotify_token' );
}
add_action("admin_init","album_page_settings");

function artist_id_markup() { ?>
    <input type="text" id="artist_id" name="artist_id" value="<?php echo get_option( 'artist_id' ); ?>">
<?php }

function spotify_token_markup() { ?>
    <input type="text" id="spotify_token" name="spotify_token" value="<?php echo get_option( 'spotify_token' ); ?>">
<?php }