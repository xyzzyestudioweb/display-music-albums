<?php 

/* Plugin Name:       Display Music Albums
* Plugin URI:        http://www.google.es
* Description:       Plugin para mostrar álbumes de música
* Version:           1.0.0
* Author:            Adrián Incera Cruz
* Author URI:        https://xyzzyestudioweb.com
* License:           GPL-2.0+
* License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
* Text Domain:       display-music-albums
* Domain Path:       /languages
*/

if(!defined('ABSPATH')) {
    return;
}

// Funciones de administración
require_once('backend/admin.php');

// Shortcode frontend
require_once('frontend/shortcode.php');