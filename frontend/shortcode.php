<?php

function get_spotify_albums($atts) {
    
    $artist_id  = get_option( 'artist_id' );
    $token      = get_option( 'spotify_token' );

    if(!empty($atts['artist']))
        $artist_id = $atts['artist'];

    if(!empty($atts['token']))
        $token = $atts['token'];

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL     => "https://api.spotify.com/v1/artists/".$artist_id."/albums",
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $token ."\"",
            "Accept: application/json"
        )
    ));

    $response = curl_exec($curl);
    $error = curl_error($curl);

    curl_close($curl);

    if($error) {
        echo "ERROR CURL: " . $error;
    } else {
        $obj = json_decode($response, false);
        foreach($obj->items as $item) {
            echo $item->name . ' // ' . $item->release_date . '<br/>';
        }
    }
}
add_shortcode("music_albums", "get_spotify_albums");