<?php 

if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

delete_option('artist_id');
delete_option('spotify_token');